from order.order import Order
from buffer.buffer_cell import BufferCell
from stats import Statistics
import pandas as pd
import sys

class Buffer:
    buffer_cells: list[BufferCell]
    stats: Statistics

    def __init__(self, capacity: int, stats: Statistics) -> None:
        self.buffer_cells = [BufferCell(i) for i in range(capacity)]
        self.stats = stats

    def __has_empty_cells(self) -> bool:
        for buffer_cell in self.buffer_cells:
            if buffer_cell.is_empty():
                return True
        return False

    def is_empty(self) -> bool:
        for buffer_cell in self.buffer_cells:
            if buffer_cell.is_empty() == False:
                return False
        return True
    
    def pop_first_order(self) -> Order:
        earliest_time = sys.float_info.max
        for buffer_cell in self.buffer_cells:
            if buffer_cell.get_order() is not None and buffer_cell.get_order().get_gen_time() < earliest_time:
                earliest_time = buffer_cell.get_order().get_gen_time()
                buffer_cell_with_earliest_order = buffer_cell
        return buffer_cell_with_earliest_order.pop_order()
    
    def add_order(self, order: Order, sys_time) -> None:
        if self.__has_empty_cells():
            for buffer_cell in self.buffer_cells:
                if buffer_cell.is_empty():
                    buffer_cell.set_order(order)
                    if self.stats.mode == 'step':
                        print(f'Set order {order.get_complex_id()} to buffer_cell: {buffer_cell.get_id()}')
                    return
        else:
            popped_order = self.pop_first_order()
            self.stats.add_refused_order(popped_order, sys_time)
            if self.stats.mode == 'step':
                print(f'Refused order: {popped_order.get_complex_id()}')
            for buffer_cell in self.buffer_cells:
                if buffer_cell.is_empty():
                    buffer_cell.set_order(order)
                    if self.stats.mode == 'step':
                        print(f'Set order {order.get_complex_id()} to buffer_cell: {buffer_cell.get_id()}')
                    return

    def to_df(self) -> pd.DataFrame:
        df = pd.DataFrame.from_records(buffer_cell.__dict__ for buffer_cell in self.buffer_cells)
        df['order'] = df['order'].apply(lambda order: order.get_complex_id() if order is not None else None)
        return df