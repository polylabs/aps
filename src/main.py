from source.source_list import SourceList
from device.device_list import DeviceList
from buffer.buffer import Buffer
from stats import Statistics
from math import log
from random import random
from config import Config
import warnings
warnings.filterwarnings("ignore")

def auto_simulation(config: dict):
    sys_time = 0
    stats = Statistics(config['source_count'], config['mode'])
    source_list = SourceList(config['source_count'], config['lambda_'], stats)
    device_list = DeviceList(config['device_count'], config['a'], config['b'], stats)
    buffer = Buffer(config['buffer_capacity'], stats)
    order_list = [source.generate_order() for source in source_list.source_list]
    generated_orders = 0

    while generated_orders < config['order_count']:
        earliest_order = order_list.pop(order_list.index(min(order_list, key=lambda x: x.gen_time)))
        stats.add_generated_order(earliest_order)
        generated_orders += 1
        sys_time = earliest_order.get_gen_time()
        device_list.manage_finished_devices(sys_time)
        if buffer.is_empty() and device_list.has_available_devices():
            device_list.set_order(earliest_order, sys_time)
        elif device_list.has_available_devices() == False:
            buffer.add_order(earliest_order, sys_time)
        else:
            while device_list.has_available_devices() and buffer.is_empty() == False:
                first_order_in_buffer = buffer.pop_first_order()
                device_list.set_order(first_order_in_buffer, sys_time)
                stats.add_order_waiting_time(first_order_in_buffer, sys_time)
            if device_list.has_available_devices():
                device_list.set_order(earliest_order, sys_time)
            else:
                buffer.add_order(earliest_order, sys_time)

        order_list.append(source_list.get_source_by_id(earliest_order.get_source_id()).generate_order())

    while (device_list.are_all_available() == False) or (buffer.is_empty() == False) and sys_time is not None:
        sys_time += (-1/config['lambda_']) * log(random())
        if sys_time is not None:
            device_list.manage_finished_devices(sys_time + 0.5)
            while buffer.is_empty() == False and device_list.has_available_devices():
                first_order_in_buffer = buffer.pop_first_order()
                device_list.set_order(first_order_in_buffer, sys_time)
                stats.add_order_waiting_time(first_order_in_buffer, sys_time)
    print(stats.to_df())
    print(device_list.get_usage_rate(sys_time))

def step_simulation(config: dict):
    sys_time = 0
    stats = Statistics(config['source_count'], config['mode'])
    source_list = SourceList(config['source_count'], config['lambda_'], stats)
    device_list = DeviceList(config['device_count'], config['a'], config['b'], stats)
    buffer = Buffer(config['buffer_capacity'], stats)
    order_list = [source.generate_order() for source in source_list.source_list]
    generated_orders = 0

    while generated_orders < config['order_count']:
        earliest_order = order_list.pop(order_list.index(min(order_list, key=lambda x: x.gen_time)))
        stats.add_generated_order(earliest_order)
        generated_orders += 1
        sys_time = earliest_order.get_gen_time()
        print('='*80)
        print(f'Device list:\n {device_list.to_df()}')
        print(f'Buffer:\n {buffer.to_df()}')
        print(sys_time)
        device_list.manage_finished_devices(sys_time)
        print(f'Generated order {earliest_order.get_complex_id()} at {earliest_order.get_gen_time()}')
        if buffer.is_empty() and device_list.has_available_devices():
            device_list.set_order(earliest_order, sys_time)
        elif device_list.has_available_devices() == False:
            buffer.add_order(earliest_order, sys_time)
        else:
            while device_list.has_available_devices() and buffer.is_empty() == False:
                first_order_in_buffer = buffer.pop_first_order()
                device_list.set_order(first_order_in_buffer, sys_time)
                stats.add_order_waiting_time(first_order_in_buffer, sys_time)
            if device_list.has_available_devices():
                device_list.set_order(earliest_order, sys_time)
            else:
                buffer.add_order(earliest_order, sys_time)
        print(stats.to_df())
        order_list.append(source_list.get_source_by_id(earliest_order.get_source_id()).generate_order())

    while (device_list.are_all_available() == False) or (buffer.is_empty() == False) and sys_time is not None:
        sys_time += (-1/config['lambda_']) * log(random())
        if sys_time is not None:
            print('='*80)
            print(f'Device list:\n {device_list.to_df()}')
            print(f'Buffer:\n {buffer.to_df()}')
            input()
            device_list.manage_finished_devices(sys_time + 0.5)
            while buffer.is_empty() == False and device_list.has_available_devices():
                first_order_in_buffer = buffer.pop_first_order()
                device_list.set_order(first_order_in_buffer, sys_time)
                stats.add_order_waiting_time(first_order_in_buffer, sys_time)
                print('='*80)
    print(stats.to_df())
    print(device_list.get_usage_rate(sys_time))

def main():
    config = Config().to_dict()
    if config['mode'] == 'step':
        step_simulation(config)
    elif config['mode'] == 'auto':
        auto_simulation(config)
    else:
        raise 'Invalid mode'

if __name__ == '__main__':
    main()